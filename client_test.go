package tickstore_go_client

import (
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	"gitlab.com/alphaticks/tickstore-go-client/config"
	types "gitlab.com/alphaticks/tickstore-types"
	"gitlab.com/alphaticks/tickstore-types/tickobjects"
	"testing"
	"time"
	"unsafe"
)

var cfg = config.StoreClient{Address: "127.0.0.1:3550", Insecure: true}

func TestClient_DeleteMeasurement(t *testing.T) {
	c, err := NewRemoteClient(cfg)
	if err != nil {
		t.Fatalf("error creating client: %v", err)
	}
	if err := c.RegisterMeasurement("counter", "UInt64"); err != nil {
		t.Fatalf("error registering measurement: %v", err)
	}

	tags := map[string]string{
		"id": "1",
	}
	w, err := c.NewTickWriter("counter", tags, 20*time.Millisecond)
	if err != nil {
		t.Fatalf("error getting measurement: %v", err)
	}

	startTick := uint64(time.Now().UnixNano() / 1000000)
	tick := startTick

	object := tickobjects.UInt64(0)

	if err := w.WriteObject(tick, &object); err != nil {
		t.Fatalf("error writing tick object: %v", err)
	}

	if err := w.Flush(); err != nil {
		t.Fatalf("error flushing writer: %v", err)
	}

	if err := c.DeleteMeasurement("counter", tags, tick, tick); err == nil {
		t.Fatalf("should have got an error deleting measurement")
	}

	if err := w.Close(); err != nil {
		t.Fatalf("error closing writer: %v", err)
	}

	time.Sleep(1 * time.Second)

	if err := c.DeleteMeasurement("counter", tags, tick, tick); err != nil {
		t.Fatalf("error deleting measurement: %v", err)
	}
}

func TestWrite(t *testing.T) {
	c, err := NewRemoteClient(cfg)
	if err != nil {
		t.Fatalf("error creating client: %v", err)
	}

	if err := c.RegisterMeasurement("counter", "UInt64"); err != nil {
		t.Fatalf("error registering measurement: %v", err)
	}

	w, err := c.NewTickWriter("counter", map[string]string{
		"id": "1",
	}, 20*time.Millisecond)
	if err != nil {
		t.Fatalf("error getting measurement: %v", err)
	}

	startTick := uint64(time.Now().UnixNano() / 1000000)
	tick := startTick

	object := tickobjects.UInt64(0)

	if err := w.WriteObject(tick, &object); err != nil {
		t.Fatalf("error writing tick object: %v", err)
	}

	for i := 1; i < 10000; i++ {
		tick += 1
		slice := []int64{int64(i)}
		deltas := gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&slice[0]),
			Len:     1,
		}
		if err := w.WriteDeltas(tick, deltas); err != nil {
			t.Fatalf("error writing tick object: %v", err)
		}
	}

	if err := w.Close(); err != nil {
		t.Fatalf("error closing writers: %v", err)
	}
	time.Sleep(1 * time.Second)

	qs := types.NewQuerySettings(
		types.WithStreaming(false),
		types.WithFrom(startTick),
		types.WithTo(tick-1),
		types.WithSelector(`SELECT counter WHERE id="1"`))

	q, err := c.NewQuery(qs)
	if err != nil {
		t.Fatalf("error querying tickstore: %v", err)
	}

	lastTick := startTick - 1
	var lastCounter int64 = -1
	for q.Next() {
		tick, object, _ := q.Read()
		counter := int64(*object.(*tickobjects.UInt64))
		if lastTick+1 != tick || lastCounter+1 != counter {
			fmt.Println(lastTick, tick)
			fmt.Println(lastCounter, counter)
			t.Fatalf("error")
		}
		lastTick = tick
		lastCounter = counter
	}
	fmt.Println(q.Err())
}

func TestStreamingQuery(t *testing.T) {
	c, err := NewRemoteClient(cfg)
	if err != nil {
		t.Fatalf("error creating client: %v", err)
	}

	if err := c.RegisterMeasurement("counter", "UInt64"); err != nil {
		t.Fatalf("error registering measurement: %v", err)
	}

	startTick := uint64(time.Now().UnixNano() / 1000000)

	// We set a batch size of 1 so that query returns as soon as we wrote the measurement
	qs := types.NewQuerySettings(
		types.WithStreaming(true),
		types.WithBatchSize(1),
		types.WithFrom(startTick),
		types.WithTo(startTick*2),
		types.WithSelector(`SELECT counter WHERE id="1"`))
	q, err := c.NewQuery(qs)
	if err != nil {
		t.Fatalf("error querying tickstore: %v", err)
	}

	w, err := c.NewTickWriter("counter", map[string]string{
		"id": "1",
	}, 20*time.Millisecond)
	if err != nil {
		t.Fatalf("error getting measurement: %v", err)
	}

	tick := startTick

	object := tickobjects.UInt64(0)

	for i := 0; i < 100; i++ {
		if err := w.WriteObject(tick, &object); err != nil {
			t.Fatalf("error writing tick object: %v", err)
		}

		// When flush returns, guaranteed that write occurred.
		if err := w.Flush(); err != nil {
			t.Fatalf("error flushing writer: %v", err)
		}
		if !q.Next() {
			t.Fatalf("query was supposed to Next, didn't: %v", q.Err())
		}

		qTick, qObject, _ := q.Read()
		counter := *qObject.(*tickobjects.UInt64)
		if qTick != tick || counter != object {
			fmt.Println(qTick, tick)
			fmt.Println(counter, object)
			//t.Fatalf("error")
		}

		tick += 1
		object += 1
	}

	if err := w.Close(); err != nil {
		t.Fatalf("error closing writers: %v", err)
	}

	if err := q.Close(); err != nil {
		t.Fatalf("error closing query: %v", err)
	}
}

func TestClient_LastEventTime(t *testing.T) {
	c, err := NewRemoteClient(cfg)
	if err != nil {
		t.Fatalf("error creating client: %v", err)
	}
	if err := c.RegisterMeasurement("counter", "UInt64"); err != nil {
		t.Fatalf("error registering measurement: %v", err)
	}
	w, err := c.NewTickWriter("counter", map[string]string{
		"id": "1",
	}, 20*time.Millisecond)
	if err != nil {
		t.Fatalf("error getting measurement: %v", err)
	}

	tick := uint64(time.Now().UnixNano() / 1000000)

	object := tickobjects.UInt64(0)
	if err := w.WriteObject(tick, &object); err != nil {
		t.Fatalf("error writing tick object: %v", err)
	}
	if err := w.Close(); err != nil {
		t.Fatal(err)
	}

	lt, err := c.GetLastEventTime("counter", map[string]string{
		"id": "1",
	})
	if lt != tick {
		t.Fatalf("was expecting %d, got %d", tick, lt)
	}
}
