module gitlab.com/alphaticks/tickstore-go-client

go 1.18

require (
	github.com/melaurent/gotickfile/v2 v2.0.0-20230926083754-40190a452b15
	gitlab.com/alphaticks/tickstore-grpc v0.0.0-20220513081230-2364b7f3c9bb
	gitlab.com/alphaticks/tickstore-types v0.0.0-20230424110855-384f0f94c9a1
	google.golang.org/grpc v1.35.0
)

require (
	cloud.google.com/go v0.72.0 // indirect
	cloud.google.com/go/storage v1.12.0 // indirect
	github.com/golang-collections/collections v0.0.0-20130729185459-604e922904d3 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/googleapis/gax-go/v2 v2.0.5 // indirect
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/melaurent/gotickfile v0.0.0-20220126102058-08e9bfcfc230 // indirect
	github.com/melaurent/kafero v1.2.4-0.20210921082217-5279763aa403 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/wangjia184/sortedset v0.0.0-20220209072355-af6d6d227aa7 // indirect
	gitlab.com/alphaticks/gorderbook v0.0.0-20230920111105-fcc11cc1f4b2 // indirect
	go.opencensus.io v0.22.5 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/mod v0.6.0-dev.0.20211013180041-c96bc1413d57 // indirect
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d // indirect
	golang.org/x/oauth2 v0.0.0-20201109201403-9fd604954f58 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.8-0.20211029000441-d6a9af8af023 // indirect
	golang.org/x/xerrors v0.0.0-20220517211312-f3a8303e98df // indirect
	google.golang.org/api v0.36.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20201201144952-b05cb90ed32e // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)


//replace gitlab.com/alphaticks/tickstore-types => /home/lmeunier/Documents/alphaticks/tickstore-types
