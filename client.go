package tickstore_go_client

import (
	"context"
	"fmt"
	"gitlab.com/alphaticks/tickstore-go-client/config"
	"gitlab.com/alphaticks/tickstore-go-client/query"
	"gitlab.com/alphaticks/tickstore-go-client/writers"
	tickstore_grpc "gitlab.com/alphaticks/tickstore-grpc"
	types "gitlab.com/alphaticks/tickstore-types"
	"gitlab.com/alphaticks/tickstore-types/tickobjects"
	"google.golang.org/grpc"
	"math"
	"reflect"
	"regexp"
	"sync"
	"time"
)

type RemoteClient struct {
	sync.RWMutex
	address      string
	client       tickstore_grpc.StoreClient
	Measurements map[string]string
}

func NewRemoteClient(cfg config.StoreClient, opts ...grpc.DialOption) (*RemoteClient, error) {
	opts = append(opts, grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(math.MaxInt64)))
	cr, err := cfg.GetDialOption()
	if err != nil {
		return nil, err
	}
	opts = append(opts, cr)
	conn, err := grpc.Dial(cfg.Address, opts...)
	if err != nil {
		return nil, fmt.Errorf("error connecting to tickstore gRPC endpoint: %v", err)
	}
	client := tickstore_grpc.NewStoreClient(conn)

	measurements := make(map[string]string)
	res, err := client.GetMeasurements(context.Background(), &tickstore_grpc.GetMeasurementsRequest{})
	if err != nil {
		return nil, fmt.Errorf("error fetching measurements: %v", err)
	}
	for _, m := range res.Measurements {
		if _, _, ok := tickobjects.GetTickObject(m.TypeID); ok {
			measurements[m.Name] = m.TypeID
		}
	}

	return &RemoteClient{
		address:      cfg.Address,
		client:       client,
		Measurements: measurements,
	}, nil
}

func (c *RemoteClient) RegisterMeasurement(measurement string, typeID string) error {
	c.Lock()
	defer c.Unlock()
	if _, _, ok := tickobjects.GetTickObject(typeID); ok {
		c.Measurements[measurement] = typeID
	} else {
		return fmt.Errorf("unknown type %s", typeID)
	}

	_, err := c.client.RegisterMeasurement(context.Background(), &tickstore_grpc.RegisterMeasurementRequest{
		Measurement: &tickstore_grpc.Measurement{
			Name:   measurement,
			TypeID: typeID,
		},
	})
	if err != nil {
		delete(c.Measurements, measurement)
		return err
	}

	return nil
}

func (c *RemoteClient) DeleteMeasurement(measurement string, tags map[string]string, from, to uint64) error {
	_, err := c.client.DeleteMeasurement(context.Background(), &tickstore_grpc.DeleteMeasurementRequest{
		Measurement: measurement,
		Tags:        tags,
		From:        from,
		To:          to,
	})

	return err
}

func (c *RemoteClient) GetLastEventTime(measurement string, tags map[string]string) (uint64, error) {
	res, err := c.client.GetLastEventTime(context.Background(), &tickstore_grpc.GetLastEventTimeRequest{
		Measurement: measurement,
		Tags:        tags,
	})

	if err != nil {
		return 0, err
	} else {
		return res.Tick, err
	}
}

func (c *RemoteClient) NewTickWriter(measurement string, tags map[string]string, flushTime time.Duration) (types.TickstoreWriter, error) {
	writer, err := c.client.Write(context.Background())
	if err != nil {
		return nil, fmt.Errorf("error creating writers: %v", err)
	}

	if typ, ok := c.Measurements[measurement]; ok {
		_, deltaType, ok := tickobjects.GetTickObject(typ)
		if !ok {
			return nil, fmt.Errorf("unknown type %s", typ)
		}
		tickWriter, err := writers.NewTickWriter(measurement, deltaType, tags, writer, flushTime)
		if err != nil {
			return nil, fmt.Errorf("error creating tick writers: %v", err)
		}

		return tickWriter, err
	} else {
		return nil, fmt.Errorf("unknown measurement %s", measurement)
	}
}

func (c *RemoteClient) NewQuery(qs *types.QuerySettings) (types.TickstoreQuery, error) {
	request := &tickstore_grpc.StoreQueryRequest{
		Streaming: qs.Streaming,
		BatchSize: qs.BatchSize,
		Timeout:   qs.Timeout,
		Selector:  qs.Selector,
		From:      qs.From,
		To:        qs.To,
	}
	if qs.Sampler != nil {
		if sampler, ok := qs.Sampler.(types.TickSampler); ok {
			request.Sampler = &tickstore_grpc.StoreQueryRequest_TickSampler{TickSampler: &tickstore_grpc.TickSampler{Interval: sampler.Interval}}
		} else {
			return nil, fmt.Errorf("unknown sampler type")
		}
	}
	ctx, canc := context.WithCancel(context.Background())
	fmt.Println("REQ", request)
	reader, err := c.client.Query(ctx, request)
	if err != nil {
		canc()
		return nil, fmt.Errorf("error querying tickstore: %v", err)
	}

	var objectType reflect.Type
	var deltaType reflect.Type
	var q types.TickstoreQuery

	if qs.ObjectType == "" {
		ok := false

		reg, err := regexp.Compile(`^SELECT ([A-Za-z_0-9\-]*)(\(|\s|$)`)
		if err != nil {
			canc()
			return nil, err
		}
		strs := reg.FindStringSubmatch(qs.Selector)
		if len(strs) < 2 {
			canc()
			return nil, fmt.Errorf("object not found in selector string")
		}
		tickObject := strs[1]
		objectType, deltaType, ok = tickobjects.GetTickObject(tickObject)
		if !ok {
			typ, ok := c.Measurements[tickObject]
			if ok {
				objectType, deltaType, ok = tickobjects.GetTickObject(typ)
				if !ok {
					canc()
					return nil, fmt.Errorf("unknown tick object type %s for query", typ)
				}
			} else {
				canc()
				return nil, fmt.Errorf("unknown tick object %s for query", tickObject)
			}
		}

		q = query.NewQuery(reader, canc, objectType, deltaType)

	} else {
		ok := false
		objectType, deltaType, ok = tickobjects.GetTickObject(qs.ObjectType)
		if !ok {
			canc()
			return nil, fmt.Errorf("unknown tick object for query")
		}

		q = query.NewQuery(reader, canc, objectType, deltaType)
	}

	return q, nil
}
