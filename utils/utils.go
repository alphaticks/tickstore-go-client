package utils

import (
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"sort"
)

func HashTags(tags map[string]string) uint64 {
	var keys []string
	for k := range tags {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	str := ""
	for _, k := range keys {
		str += fmt.Sprintf("&%s=%s", k, tags[k])
	}
	hashBytes := md5.Sum([]byte(str))
	ID := binary.LittleEndian.Uint64(hashBytes[:8])
	return ID
}

func FormatTags(tags map[string]string) string {
	var keys []string
	for k := range tags {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	str := ""
	for _, k := range keys {
		str += fmt.Sprintf(`%s="%s" `, k, tags[k])
	}
	return str[:len(str)-1]
}
