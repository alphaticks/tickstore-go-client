package query

import (
	"context"
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	grpc "gitlab.com/alphaticks/tickstore-grpc"
	types "gitlab.com/alphaticks/tickstore-types"
	"gitlab.com/alphaticks/tickstore-types/tickobjects"
	"reflect"
	"time"
	"unsafe"
)

// A query object allows you to traverse sequentially.
// One tick object per query ? One tick object per reader ? If there is a group by
// then supposed to have a tick object per query

type Query struct {
	client     grpc.Store_QueryClient
	deadline   *time.Time
	canc       context.CancelFunc
	objectType reflect.Type
	deltaType  reflect.Type
	objects    map[uint64]tickobjects.TickObject
	lastDeltas map[uint64]*gotickfile.TickDeltas
	tags       map[uint64]map[string]string
	groupID    uint64
	tick       uint64
	batch      *grpc.StoreQueryBatch
	batchIdx   int
	err        error
	msg        chan interface{}
}

type Selector struct {
	Functors []Functor
}

type Functor struct {
	Apply string
	Args  []string
}

func NewQuery(client grpc.Store_QueryClient, canc context.CancelFunc, objectType, deltaType reflect.Type) types.TickstoreQuery {
	q := &Query{
		client:     client,
		canc:       canc,
		objectType: objectType,
		deltaType:  deltaType,
		objects:    make(map[uint64]tickobjects.TickObject),
		lastDeltas: make(map[uint64]*gotickfile.TickDeltas),
		tags:       make(map[uint64]map[string]string),
		tick:       0,
		batch:      nil,
		batchIdx:   0,
		err:        nil,
		msg:        make(chan interface{}),
	}

	go func() {
		for {
			msg, err := client.Recv()
			if err != nil {
				q.msg <- err
				close(q.msg)
				return
			} else {
				q.msg <- msg
			}
		}
	}()

	return q
}

func (q *Query) SetNextDeadline(deadline time.Time) {
	q.deadline = &deadline
}

// Prepare the next result row for reading with Scan
func (q *Query) Next() bool {
	if q.err != nil {
		return false
	}

	// If we have already read all the events, fetch a new batch

	defer func() { q.deadline = nil }()

	for q.batch == nil {
		var msg interface{}
		var ok bool

		if q.deadline != nil {
			select {
			case <-time.After(q.deadline.Sub(time.Now())):
				return false

			case msg, ok = <-q.msg:
				if !ok {
					return false
				}
			}
		} else {
			msg, ok = <-q.msg
			if !ok {
				return false
			}
		}

		switch res := msg.(type) {
		case *grpc.StoreQueryBatch:
			if len(res.Ticks) > 0 {
				q.batch = res
				q.batchIdx = 0
			}
		case error:
			q.err = res
		}
	}

	event := q.batch.Ticks[q.batchIdx]
	deltas := event.GetDeltas()
	q.tick = event.Tick
	q.groupID = event.GroupId

	var object tickobjects.TickObject

	if obj, ok := q.objects[q.groupID]; ok {
		object = obj
	} else {
		object = reflect.New(q.objectType).Interface().(tickobjects.TickObject)
		q.objects[q.groupID] = object
	}

	change := false
	if deltas != nil {
		l := len(deltas) / int(q.deltaType.Size())
		if l > 0 {
			deltas := gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&deltas[0]),
				Len:     l,
			}
			q.lastDeltas[q.groupID] = &deltas
			if err := object.ProcessDeltas(deltas); err != nil {
				q.err = fmt.Errorf("error processing deltas: %v", err)
				return false
			}
			change = true
		}
	} else {
		snapshot := event.GetSnapshot()
		if snapshot != nil {
			// check if we have a tickfunctor so we can get the deltas
			if tf, ok := object.(tickobjects.TickFunctor); ok {
				dlts, err := tf.ApplySnapshot(snapshot.Snapshot, event.Tick, event.GroupId)
				if err != nil {
					q.err = fmt.Errorf("error processing snapshot: %v", err)
					return false
				}
				q.lastDeltas[q.groupID] = dlts
			} else {
				if err := object.FromSnapshot(snapshot.Snapshot); err != nil {
					q.err = fmt.Errorf("error processing snapshot: %v", err)
					return false
				}
				q.lastDeltas[q.groupID] = nil
			}

			q.tags[q.groupID] = snapshot.Tags
			change = true
		}
	}
	q.batchIdx += 1

	if q.batchIdx == len(q.batch.Ticks) {
		q.batch = nil
		q.batchIdx = -1
	}
	if !change {
		return q.Next()
	}

	return true
}

func (q *Query) Progress(end uint64) bool {
	if q.err != nil {
		return false
	}

	if q.tick >= end {
		return true
	}

	// If we have already read all the events, fetch a new batch
	for q.batch == nil {
		msg, err := q.client.Recv()
		if err != nil {
			q.err = err
			return false
		}

		if len(msg.Ticks) > 0 {
			q.batch = msg
			q.batchIdx = 0
		}
	}
	event := q.batch.Ticks[q.batchIdx]

	for event.Tick < end {

		q.tick = event.Tick
		q.groupID = event.GroupId
		deltas := event.GetDeltas()

		var object tickobjects.TickObject

		if obj, ok := q.objects[q.groupID]; ok {
			object = obj
		} else {
			object = reflect.New(q.objectType).Interface().(tickobjects.TickObject)
			q.objects[q.groupID] = object
		}

		if deltas != nil {
			l := len(deltas) / int(q.deltaType.Size())
			deltas := gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&deltas[0]),
				Len:     l,
			}
			q.lastDeltas[q.groupID] = &deltas
			if err := object.ProcessDeltas(deltas); err != nil {
				q.err = fmt.Errorf("error processing deltas: %v", err)
				return false
			}
		} else {
			snapshot := event.GetSnapshot()
			if snapshot != nil {
				// check if we have a tickfunctor so we can get the deltas
				if tf, ok := object.(tickobjects.TickFunctor); ok {
					dlts, err := tf.ApplySnapshot(snapshot.Snapshot, event.Tick, event.GroupId)
					if err != nil {
						q.err = fmt.Errorf("error processing snapshot: %v", err)
						return false
					}
					q.lastDeltas[q.groupID] = dlts
				} else {
					if err := object.FromSnapshot(snapshot.Snapshot); err != nil {
						q.err = fmt.Errorf("error processing snapshot: %v", err)
						return false
					}
					q.lastDeltas[q.groupID] = nil
				}

				q.tags[q.groupID] = snapshot.Tags
			}
		}
		q.batchIdx += 1

		if q.batchIdx == len(q.batch.Ticks) {
			q.batch = nil
			q.batchIdx = -1
		}

		// If we have already read all the events, fetch a new batch
		for q.batch == nil {
			msg, err := q.client.Recv()
			if err != nil {
				q.err = err
				return false
			}

			if len(msg.Ticks) > 0 {
				q.batch = msg
				q.batchIdx = 0
			}
		}
		event = q.batch.Ticks[q.batchIdx]
	}

	return true
}

// TODO tags
func (q *Query) Read() (uint64, tickobjects.TickObject, uint64) {
	return q.tick, q.objects[q.groupID], q.groupID
}

// TODO
func (q *Query) ReadDeltas() (uint64, *gotickfile.TickDeltas, uint64) {
	return q.tick, q.lastDeltas[q.groupID], q.groupID
}

func (q *Query) Tags() map[string]string {
	return q.tags[q.groupID]
}

func (q *Query) Close() error {
	// call the cancel function
	q.canc()
	return q.err
}

func (q *Query) DeltaType() reflect.Type {
	return q.deltaType
}

func (q *Query) Err() error {
	return q.err
}
