package config

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"io/ioutil"
)

type StoreClient struct {
	Address                    string
	Insecure                   bool
	ClientCertificatePublic    string
	ClientCertificatePrivate   string
	AuthorityCertificatePublic string
}

func (c StoreClient) GetDialOption() (grpc.DialOption, error) {
	if c.Insecure {
		return grpc.WithInsecure(), nil
	} else {
		cfg := &tls.Config{}
		if c.ClientCertificatePublic != "" {
			// Client authentication
			cert, err := tls.LoadX509KeyPair(c.ClientCertificatePublic, c.ClientCertificatePrivate)
			if err != nil {
				return nil, fmt.Errorf("error loading client certificate: %v", err)
			}
			cfg.Certificates = []tls.Certificate{cert}
		}

		if c.AuthorityCertificatePublic != "" {
			// Create a certificate pool from the certificate authority
			certPool := x509.NewCertPool()
			ca, err := ioutil.ReadFile(c.AuthorityCertificatePublic)
			if err != nil {
				return nil, fmt.Errorf("could not read ca certificate: %s", err)
			}

			// Append the certificates from the CA
			if ok := certPool.AppendCertsFromPEM(ca); !ok {
				return nil, fmt.Errorf("failed to append ca certs")
			}
			cfg.RootCAs = certPool
		}

		creds := credentials.NewTLS(cfg)
		return grpc.WithTransportCredentials(creds), nil
	}
}
