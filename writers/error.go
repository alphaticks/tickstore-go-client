package writers

import "errors"

var (
	ErrTickOutOfOrder = errors.New("tick out of order not supported")
)
