package writers

import (
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	grpc "gitlab.com/alphaticks/tickstore-grpc"
	"gitlab.com/alphaticks/tickstore-types/tickobjects"
	"reflect"
	"sync"
	"time"
	"unsafe"
)

var ErrProcessingDelta *ErrProcessingDeltaType = &ErrProcessingDeltaType{}

// ErrProcessingDeltaType is a custom error type that can wrap an underlying error.
type ErrProcessingDeltaType struct {
	cause error
}

// NewErrProcessingDelta creates a new instance of ErrProcessingDelta wrapping the provided cause.
func NewErrProcessingDelta(cause error) error {
	return &ErrProcessingDeltaType{cause: cause}
}

// Error implements the error interface for ErrProcessingDelta.
func (e *ErrProcessingDeltaType) Error() string {
	return fmt.Sprintf("processing delta: %v", e.cause)
}

// Unwrap reveals the underlying error.
func (e *ErrProcessingDeltaType) Unwrap() error {
	return e.cause
}

type TickWriter struct {
	measurement string
	deltaType   reflect.Type
	tags        map[string]string
	writer      grpc.Store_WriteClient
	tickObject  tickobjects.TickObject
	batch       *grpc.TickEventBatch
	lastTick    uint64
	err         error
	fl          sync.Mutex
	sync.Mutex
}

// Protos with request write batch and wait for response. If error just krash
func NewTickWriter(measurement string, deltaType reflect.Type, tags map[string]string, wc grpc.Store_WriteClient, flushTime time.Duration) (*TickWriter, error) {
	batch := &grpc.TickEventBatch{
		Events: nil,
	}

	tw := &TickWriter{
		measurement: measurement,
		deltaType:   deltaType,
		tags:        tags,
		writer:      wc,
		tickObject:  nil,
		batch:       batch,
		lastTick:    0,
		err:         nil,
	}

	// Flusher
	go func() {
		ticker := time.NewTicker(flushTime)
		for range ticker.C {
			_ = tw.Flush()
		}
	}()

	return tw, nil
}

func (w *TickWriter) WriteObject(tick uint64, object tickobjects.TickObject) error {
	w.Lock()

	if w.err != nil {
		w.Unlock()
		return w.err
	}

	if w.tickObject == nil {
		// We have no choice but to send the snapshot to the database
		w.tickObject = object.Clone()
		snapshot := object.ToSnapshot()
		event := &grpc.TickEvent{
			Tick: tick,
			Event: &grpc.TickEvent_Snapshot{
				Snapshot: &grpc.TickSnapshot{
					Measurement: w.measurement,
					Tags:        w.tags,
					Snapshot:    snapshot,
				},
			},
		}
		w.batch.Events = append(w.batch.Events, event)

	} else {
		deltas, err := w.tickObject.DeltasTo(object)
		if err != nil {
			w.Unlock()
			return fmt.Errorf("error computing deltas: %v", err)
		}

		if deltas.Len == 0 {
			w.Unlock()
			return nil
		}

		buff := make([]byte, int(w.deltaType.Size())*deltas.Len)
		ptr := uintptr(deltas.Pointer)
		for i := 0; i < len(buff); i++ {
			buff[i] = *(*byte)(unsafe.Pointer(ptr + uintptr(i)))
		}
		event := &grpc.TickEvent{
			Tick:  tick,
			Event: &grpc.TickEvent_Deltas{Deltas: buff},
		}

		if err := w.tickObject.ProcessDeltas(deltas); err != nil {
			w.Unlock()
			return fmt.Errorf("error processing deltas: %v", err)
		}

		w.batch.Events = append(w.batch.Events, event)
	}

	w.Unlock()
	// Heuristic to prevent too big messages by blocking writer
	if len(w.batch.Events)*int(w.deltaType.Size()) > 400000 {
		return w.Flush()
	} else {
		return nil
	}
}

func (w *TickWriter) WriteDeltas(tick uint64, deltas gotickfile.TickDeltas) error {
	w.Lock()
	if w.err != nil {
		w.Unlock()
		return w.err
	}

	if deltas.Len > 0 {
		if w.tickObject == nil {
			w.Unlock()
			return fmt.Errorf("error writing deltas without tick object")
		}
		buff := make([]byte, int(w.deltaType.Size())*deltas.Len)
		for i := 0; i < len(buff); i++ {
			buff[i] = *(*byte)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(i)))
		}

		event := &grpc.TickEvent{
			Tick:  tick,
			Event: &grpc.TickEvent_Deltas{Deltas: buff},
		}

		if err := w.tickObject.ProcessDeltas(deltas); err != nil {
			w.Unlock()
			return NewErrProcessingDelta(err)
		}
		w.batch.Events = append(w.batch.Events, event)
	} else {
		if w.tickObject == nil {
			// skip heart beat if tick object nil and writer not yet initialized
			w.Unlock()
			return nil
		}
		event := &grpc.TickEvent{
			Tick:  tick,
			Event: nil,
		}

		w.batch.Events = append(w.batch.Events, event)
	}

	w.Unlock()
	// Heuristic to prevent too big messages by blocking writer
	if len(w.batch.Events)*int(w.deltaType.Size()) > 400000 {
		return w.Flush()
	} else {
		return nil
	}
}

func (w *TickWriter) Flush() error {
	if w.err != nil {
		return w.err
	}

	// Flush lock
	w.fl.Lock()
	defer w.fl.Unlock()

	// Lock batch to prevent writing while sending
	w.Lock()
	if len(w.batch.Events) == 0 {
		w.Unlock()
		return nil
	}
	idx := len(w.batch.Events)
	if err := w.writer.Send(w.batch); err != nil {
		w.err = fmt.Errorf("error writing to tickstore: %v", err)
		w.Unlock()
		return w.err
	}
	w.Unlock()

	// Wait for ack
	msg, err := w.writer.Recv()
	if err != nil {
		w.err = fmt.Errorf("error receiving Ack: %v", err)
		return err
	}
	// TODO if out of order do something ? Or better to just krash and send snapshot back ?
	if msg.Error != "" {
		w.err = fmt.Errorf("error from writer: %s", msg.Error)
		return w.err
	}

	// Remove Acked events
	w.Lock()
	i := 0
	for ; idx+i < len(w.batch.Events); i++ {
		w.batch.Events[i] = w.batch.Events[idx+i]
	}
	w.batch.Events = w.batch.Events[:i]
	w.Unlock()

	w.lastTick = msg.Tick

	return nil
}

func (w *TickWriter) GetObject() tickobjects.TickObject {
	return w.tickObject
}

func (w *TickWriter) Close() error {
	if err := w.Flush(); err != nil {
		_ = w.writer.CloseSend()
		return err
	} else {
		w.Lock()
		defer w.Unlock()
		return w.writer.CloseSend()
	}
}
